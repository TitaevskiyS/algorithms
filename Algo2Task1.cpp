/*1. ������������ ����������*/

#include <iostream>
#include <vector>

void insertion_sort(std::vector<int> &a, int n) {
	for (int i = 1; i < n; ++i) {
		int tmp = a[i];
		int j = i;
		for (; j > 0 && tmp < a[j - 1]; --j) {
			a[j] = a[j - 1];
		}
		a[j] = tmp;
	}
}

int main()
{
	std::vector<int> unsorted;
	int temp = 0;

	while (std::cin >> temp && !std::cin.eof())
		unsorted.push_back(temp);

	int n = unsorted.size();

	insertion_sort(unsorted, n);

	for (int i = 0; i < n; i++)
		std::cout << unsorted[i] << '\n';

	system("Pause");
	return 0;
}