/*7.1 ������ ������� ��� ��������� ������*/

#include <iostream>
#include <assert.h>

#define	DEF_SIZE	10

template <typename T>
class CArray
{
public:
	CArray(int size = DEF_SIZE) : buf_size(size), cur_size(0)
	{
		buffer = new T[size];
	};

	~CArray()
	{
		delete[] buffer;	
	};

	T operator [] (int i) const
	{
		assert(i < cur_size && i >= 0 && buffer != NULL);
			return buffer[i];
	};

	T& operator [] (int i)
	{
		assert(i < cur_size && i >= 0 && buffer != NULL);
		return buffer[i];
	};

	void Add(T data)
	{
		if (cur_size == buf_size)
			Grow();

		assert(buffer != NULL);
		buffer[cur_size++] = data;
	}

	int Size() const
	{
		return cur_size;
	}

	void DeleteLast()
	{
		assert(cur_size);
		--cur_size;
	}

	T Last() const
	{
		assert(cur_size);
		return buffer[cur_size - 1];
	}

private:
	void Grow()
	{
		if (cur_size == buf_size){
			buf_size *= 2;
			T* temp = new T[buf_size];
			for (int i = 0; i < cur_size; ++i)
				temp[i] = buffer[i];
			delete[] buffer;
			buffer = temp;
		}
	}

	T* buffer;
	int buf_size;
	int cur_size;
};

template <typename T>
class CHeap
{
public:
	T operator [] (int i) const
	{
		return buffer[i];
	}

	void Add(T data)
	{
		buffer.Add(data);
		int i = buffer.Size() - 1;
		while (i > 0){
			int parent = (i - 1) / 2;
			if (buffer[parent] >= buffer[i])
				return;
			std::swap(buffer[parent], buffer[i]);
			i = parent;
		}
	}

	T GetMax()
	{
		assert(buffer.Size());
		T res = buffer[0];
		buffer[0] = buffer.Last();
		buffer.DeleteLast();

		if (buffer.Size())
			Heapify(0);

		return res;
	}

	T LookMax() const
	{
		assert(buffer.Size());
		return buffer[0];
	}

	void BuildHeap()
	{
		for (int i = buffer.Size() / 2 - 1; i >= 0; --i)
			Heapify(i);
	}

	int Size() const
	{
		return buffer.Size();
	}

private:
	CArray<T> buffer;

	void Heapify(int i)
	{
		int left = 2 * i + 1;
		int right = 2 * i + 2;
		//����� ������������� �������
		int largest = i;
		if (left < buffer.Size() && buffer[left] > buffer[i])
			largest = left;
		if (right < buffer.Size() && buffer[right] > buffer[largest])
			largest = right;
		if (largest != i){
			std::swap(buffer[largest], buffer[i]);
			Heapify(largest);
		}
	}
};

int Greed(CHeap<int>& basket, int K)
{
	CArray<int> taked(basket.Size());						//������ ������
	int basket_weight = 0;
	bool conscience = false;								//������� ����

	while (basket.Size() > 0){								//����� ����� � �������
		basket_weight += basket.LookMax();
		taked.Add(basket.GetMax());
	}
	while (taked.Size() > 0){
		basket.Add(taked.Last());							//����������� �������
		taked.DeleteLast();
	}

	while (!conscience){
		int cur_weight = 0;
		bool full_hands = false;

		while (!full_hands && !conscience){								//�������� ������ ���� �����
			if (cur_weight + basket.LookMax() <= K){
				cur_weight += basket.LookMax();
				taked.Add(basket.GetMax());
			}
			else
				full_hands = true;

			if (cur_weight >= (basket_weight - basket_weight / 2))
				conscience = true;							//������ �����
		}

		if (!conscience)
			for (int i = 0; i < taked.Size(); ++i){
				basket_weight -= (taked[i] - taked[i] / 2);
				taked[i] /= 2;								//������� ������
			}

		while (taked.Size() > 0){
			if (taked.Last())
				basket.Add(taked.Last());					//����������� �������
			taked.DeleteLast();
		}
	}

	return basket_weight;;
}

int main()
{
	CHeap<int> basket;
	
	int n = 0;	//���������� �������
	std::cin >> n;

	for (int i = 0; i < n; i++){
		int temp = 0;
		std::cin >> temp;
		basket.Add(temp);
	}

	int K = 0; //����������������
	std::cin >> K;

	if (n && K && K >= basket.LookMax())
		std::cout << Greed(basket, K) << '\n';

	//system("Pause");
	return 0;
}