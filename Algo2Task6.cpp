#include <iostream>

#define RAD 64

using namespace std;

int Digit(long long a, int d)
{
	return (a >> d) & 1;
}

void Quicksort_bin(long long* a, int left, int right, int w)
{
	int i = left;
	int j = right;

	if (right <= left || w < 0)
		return;

	while (i != j)
	{
		while (Digit(a[i], w) == 0 && i < j)
			++i;
		while (Digit(a[j], w) == 1 && i < j)
			--j;

		if (i < j)
			swap(a[i], a[j]);
	}

	if (Digit(a[right], w) == 0)
		++j;

	Quicksort_bin(a, left, j - 1, w - 1);
	Quicksort_bin(a, j, right, w - 1);
}

int main()
{
	size_t n;

	cin >> n;

	long long* a = new long long[n];

	for (size_t i = 0; i < n; i++)
		cin >> a[i];

	Quicksort_bin(a, 0, n - 1, RAD - 1);

	for (size_t i = 0; i < n; i++)
		cout << a[i] << ' ';

	//cout << '\n';
	//system("Pause");
	return 0;
}