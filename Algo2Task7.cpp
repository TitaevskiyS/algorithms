#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <time.h>
#include <vector>

#define MAXSTACK	2048	// ������������ ������ �����

using namespace std;

void Quick_sort(vector<long>& a)
{
	long lbstack[MAXSTACK];
	long rbstack[MAXSTACK];

	long stackpos = 1;   // ������� ������� �����

	lbstack[stackpos] = 0;
	rbstack[stackpos] = a.size() - 1;

	do {
		// ����� ������� lb � rb �������� ������� �� �����.

		long lb = lbstack[stackpos];
		long rb = rbstack[stackpos];
		stackpos--;

		do {
			long i = lb;
			long j = rb;
			long pivot = a[rand() % (rb - lb + 1) + lb];

			do {
				while (a[i] < pivot) ++i;
				while (a[j] > pivot) --j;

				if (i <= j) {
					swap(a[i], a[j]);
					++i;
					--j;
				}
			}
			while (i < j);

			//��������� ����� ����� ������� � ����, ���� � ��� ������ 1 ��������
			if (j > lb) {
				stackpos++;
				lbstack[stackpos] = lb;
				rbstack[stackpos] = j;
			}
			lb = i;
		} 
		while (lb < rb); // ��������� ������, ���� � ��� ������ 1 ��������
	} 
	while (stackpos != 0);
}

int main()
{
	srand((unsigned int)time(0));

	int* a = new int [200000];

	cin >> temp;
	while (true){
		if (cin.eof())
			break;
		a.push_back(temp);
		cin >> temp;
	}

	//long start = clock();				//������ �������

	Quick_sort(a);

	long n = a.size();

	//for (long i = 0; i < n; i ++)		//����� �������
	//	printf("%d ", a[i]);
	//printf("\n");

	for (long i = 9; i < n; i += 10)	//����� 10 ���������
		printf("%d ", a[i]);

	//long end = clock();					//������ ���� � ����� �������
	//printf("\n%d\n", end - start);

	//system("Pause");
	delete[] a;
	return 0;
}