/*5.1 ������� � ������������ �������*/

#include <iostream>

#define		DEF_SIZE	10	//default size of buffer in constructor

/*Input command*/
#define		POP			2
#define		PUSH		3

class CQueue
{
public:
	CQueue(int size = DEF_SIZE);
	~CQueue();
	void Enqueue(int data);
	int Dequeue();

private:
	void AddMemory();

	int* buffer;
	int buf_size;
	int head;
	int tail;
	bool is_full;
};

CQueue::CQueue(int size) :	
	buf_size(size), 
	head(0), 
	tail(0),
	is_full(false)
{
	buffer = new int[size];
}

CQueue::~CQueue()
{
	delete[] buffer;
}

void CQueue::AddMemory()
{
	if (!is_full)
		return;

	buf_size *= 2;

	int* temp_buf = new int[buf_size];
	int cur_pos = 0;						//in temp_buf
	
	for (int i = head; i < buf_size; ++i)	//copy head
		temp_buf[cur_pos++] = buffer[i];

	for (int i = 0; i < tail; ++i)			//copy tail
		temp_buf[cur_pos++] = buffer[i];

	delete[] buffer;
	buffer = temp_buf;

	head = 0;
	tail = cur_pos;

	is_full = false;
}

void CQueue::Enqueue(int data)
{
	if (is_full)
		AddMemory();

	buffer[tail++] = data;
	tail %= buf_size;

	if (head == tail)
		is_full = true;
}

int CQueue::Dequeue()
{
	if (head == tail && !is_full)
		return -1;

	int res = buffer[head++];
	head %= buf_size;

	is_full = false;

	return res;
}

int main()
{
	CQueue my_queue;
	int count = 0;
	int command = 0;
	int val = 0;

	std::cin >> count;
	for (int i = 0; i < count; i++)
	{
		std::cin >> command >> val;

		switch (command)
		{
		case PUSH:
			my_queue.Enqueue(val);
			break;
		case POP:
			if (val != my_queue.Dequeue()){
				std::cout << "NO";
				return 0;
			}
			break;
		}
	}
	std::cout << "YES";

	//system("pause");
	return 0;
}