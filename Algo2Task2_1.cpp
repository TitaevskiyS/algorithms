/*2.1 �����*/

#include <iostream>

void bubble_sort(int *a, int n)
{
	for (int i = 0; i < n; i++)
		for (int j = i; j < n; j++)
			if (a[i] < a[j])
				std::swap(a[i], a[j]);
}

struct CBox
{
	int number;
	union{
		struct
		{
			int x;
			int y;
			int z;
		};
		int arr[3];
	};
	CBox() :number(0), x(0), y(0), z(0) {};
	CBox(int num, int a, int b, int c)
	{
		number = num;
		int temp[3] = { a, b, c };
		bubble_sort(temp, 3);

		x = temp[0];
		y = temp[1];
		z = temp[2];
	}
};

bool operator < (const CBox &a, const CBox &b)
{
	for (int i = 0; i < 3; i++)
		if (a.arr[i] > b.arr[i])
			return false;
	return true;
}

bool operator > (const CBox &a, const CBox &b)
{
	for (int i = 0; i < 3; i++)
		if (a.arr[i] < b.arr[i])
			return false;
	return true;
}

template<typename T>
void Heapify(T* arr, int i, int n)
{
	int left = 2 * i + 1;
	int right = 2 * i + 2;
	//����� ������������� �������
	int largest = i;
	if (left < n && arr[left] > arr[i])
		largest = left;
	if (right < n && arr[right] > arr[largest])
		largest = right;
	if (largest != i){
		std::swap(arr[largest], arr[i]);
		Heapify(arr, largest, n);
	}
}

template<typename T>
void Build_heap(T* arr, int n)
{
	for (int i = n / 2 - 1; i >= 0; --i)
		Heapify(arr, i, n);
}

template<typename T>
void Heap_sort(T* arr, int n)
{
	for (int i = n - 1; i > 0; --i){
		std::swap(arr[0], arr[i]);
		Heapify<T>(arr, 0, i);
	}
}

int main()
{
	int n = 0;
	std::cin >> n;

	CBox* box = new CBox[n];

	for (int i = 0; i < n; i++)
	{
		int x = 0, y = 0, z = 0;
		std::cin >> x;
		std::cin >> y;
		std::cin >> z;
		new (box + i) CBox(i, x, y, z);
	}
	Build_heap<CBox>(box, n);

	Heap_sort<CBox>(box, n);

	for (int i = 0; i < n; i++)
	{
		std::cout << box[i].number << '\n';
	}

	//system("Pause");
	return 0;
}