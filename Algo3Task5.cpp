#include <iostream>
#include <algorithm>

using namespace std;

struct CAVLNode
{
	int key;
	int height;
	int count;
	CAVLNode* left;
	CAVLNode* right;

	CAVLNode(int data) : key(data), height(1), left(NULL), right(NULL), count(1) {};
};

class CAVLTree
{
public:
	CAVLTree() : root(NULL) {};
	~CAVLTree() { Delete_subtree(root); };
	int Add(int key);
	void Remove(int key);
	int Height() const { return Height(root); };
	int Count() const { return Count(root); };
	void Look() const { Look(root); };

private:
	CAVLNode* root;

	int Add(CAVLNode*& node, int key, int cur_pos);
	void Balance(CAVLNode*& node);
	int Height(const CAVLNode* node) const { return node == NULL ? 0 : node->height; };
	int Count(const CAVLNode* node) const { return node == NULL ? 0 : node->count; };
	int Delta(const CAVLNode* node) const { return Height(node->right) - Height(node->left); };
	void Rotate_left(CAVLNode*& node);
	void Rotate_right(CAVLNode*& node);
	void Fix_height(CAVLNode*& node);
	void Fix_count(CAVLNode*& node);
	CAVLNode* Remove_min(CAVLNode*& node);
	void Remove(CAVLNode*& node, int key);
	void Delete_subtree(CAVLNode*& node);
	void Look(CAVLNode* node) const;
};

void CAVLTree::Delete_subtree(CAVLNode*& node)
{
	if (node)
	{
		Delete_subtree(node->left);
		Delete_subtree(node->right);
		delete node;
	}
}

int CAVLTree::Add(int key)
{
	return Add(root, key, 0);
}

int CAVLTree::Add(CAVLNode*& node, int key, int cur_pos)
{
	if (node == NULL) {
		node = new CAVLNode(key);
		return cur_pos + 1;
	}

	int res = 0;

	if (node->key <= key)
		res = Add(node->right, key, cur_pos + Count(node->left) + 1);
	else
		res = Add(node->left, key, cur_pos);

	Balance(node);

	return res;
}

void CAVLTree::Balance(CAVLNode*& node)
{
	Fix_height(node);
	Fix_count(node);

	const int d = Delta(node);

	if (d == 2) {
		if (Delta(node->right) == -1)
			Rotate_right(node->right);
		Rotate_left(node);
	}
	else if (d == -2) {
		if (Delta(node->left) == 1)
			Rotate_left(node->left);
		Rotate_right(node);
	}
}

void CAVLTree::Fix_height(CAVLNode*& node)
{
	node->height = max(Height(node->left), Height(node->right)) + 1;
}

void CAVLTree::Fix_count(CAVLNode*& node)
{
	node->count = Count(node->left) + Count(node->right) + 1;
}

void CAVLTree::Rotate_left(CAVLNode*& node)
{
	CAVLNode* temp = node->right;

	node->right = temp->left;
	temp->left = node;

	Fix_height(node);
	Fix_height(temp);

	Fix_count(node);
	Fix_count(temp);

	node = temp;
}

void CAVLTree::Rotate_right(CAVLNode*& node)
{
	CAVLNode* temp = node->left;

	node->left = temp->right;
	temp->right = node;

	Fix_height(node);
	Fix_height(temp);

	Fix_count(node);
	Fix_count(temp);

	node = temp;
}

void CAVLTree::Remove(int pos)
{
	Remove(root, pos);
}

void CAVLTree::Remove(CAVLNode*& node, int pos)
{
	if (node == NULL)
		return;

	int temp = Count(node->left) + 1;

	if (temp < pos) {
		Remove(node->right, pos - temp);
	}
	else if (temp > pos) {
		Remove(node->left, pos);
	}
	else {
		CAVLNode* l = node->left;
		CAVLNode* r = node->right;

		delete node;

		if (r == NULL) {
			node = l;
			return;
		}

		node = Remove_min(r);
		node->right = r;
		node->left = l;
	}

	Balance(node);
}

CAVLNode* CAVLTree::Remove_min(CAVLNode*& node)
{
	if (node->left == NULL) {
		CAVLNode* temp = node;
		node = node->right;
		return temp;
	}

	CAVLNode* res = Remove_min(node->left);
	Balance(node);

	return res;
}

void CAVLTree::Look(CAVLNode* node) const
{
	if (node)
	{
		Look(node->left);
		cout << node->key << ' ';
		Look(node->right);
	}

}

/*		CAVLTree	*/

int main()
{
	CAVLTree AVLTree;

	int n = 0;
	cin >> n;

	for (int i = 0; i < n; i++) {
		int command = 0;
		int data = 0;
		int temp = 0;

		cin >> command >> data;

		switch (command)
		{
		case 1:
			temp = AVLTree.Add(data);
			cout << AVLTree.Count() - temp << '\n';
			break;

		case 2:
			AVLTree.Remove(AVLTree.Count() - data);
			break;
		}

		//AVLTree.Look();
	}

	return 0;
}