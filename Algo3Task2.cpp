#include <iostream>

using namespace std;

struct CTreeNode
{
	int data;
	CTreeNode* left;
	CTreeNode* right;

	CTreeNode(const int& key) : data(key), left(NULL), right(NULL) {};
};

class CTree
{
public:
	CTree();
	~CTree();
	void Insert(const int& key);
	void Look();


private:
	CTreeNode* root;

	void Delete_subtree(CTreeNode*& node);
	void Look(CTreeNode* node);
	void Insert(CTreeNode*& node, const int& key);
};

CTree::CTree() : root(NULL) {};

CTree::~CTree()
{
	Delete_subtree(root);
}

void CTree::Delete_subtree(CTreeNode*& node)
{
	if (node)
	{
		Delete_subtree(node->left);
		Delete_subtree(node->right);
		delete node;
	}
}

void CTree::Insert(const int& key)
{
	Insert(root, key);
}

void CTree::Insert(CTreeNode*& node, const int& key)
{
	if (node == NULL) {
		node = new CTreeNode(key);
		return;
	}
	if (node->data <= key)
		Insert(node->right, key);
	else
		Insert(node->left, key);
}

void CTree::Look()
{
	Look(root);
}

void CTree::Look(CTreeNode* node)
{
	if (node)
	{
		cout << node->data << '\n';
		Look(node->left);
		Look(node->right);
	}

}

int main()
{
	CTree tree;

	int n = 0;
	cin >> n;

	for (int i = 0; i < n; i++)
	{
		int temp = 0;
		cin >> temp;
		tree.Insert(temp);
	}

	tree.Look();

	return 0;
}