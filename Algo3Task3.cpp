#include <iostream>
#include <queue>

using namespace std;

/*	CTree	*/

struct CTreeNode
{
	int data;
	CTreeNode* left;
	CTreeNode* right;

	CTreeNode(const int& key) : data(key), left(NULL), right(NULL) {};
};

class CTree
{
public:
	CTree()  : root(NULL) {};
	~CTree();
	void Insert(const int& key);
	int Width();

private:
	CTreeNode* root;

	void Delete_subtree(CTreeNode*& node);
	void Insert(CTreeNode*& node, const int& key);
};

CTree::~CTree()
{
	Delete_subtree(root);
}

void CTree::Delete_subtree(CTreeNode*& node)
{
	if (node)
	{
		Delete_subtree(node->left);
		Delete_subtree(node->right);
		delete node;
	}
}

void CTree::Insert(const int& key)
{
	Insert(root, key);
}

void CTree::Insert(CTreeNode*& node, const int& key)
{
	if (node == NULL) {
		node = new CTreeNode(key);
		return;
	}
	if (node->data <= key)
		Insert(node->right, key);
	else
		Insert(node->left, key);
}

int CTree::Width()
{
	if (root == NULL)
		return 0;

	int max_width = 1;
	int lvl1 = 1;			//���������� ����� �� ������� ������
	int lvl2 = 0;			//���������� ����� �� ��������� ������
	int cur_lvl = 0;		//������� ����� �������� ������

	queue<CTreeNode*> q;
	q.push(root);

	while (!q.empty()) {
		CTreeNode* node = q.front();
		q.pop();

		if (node->left != NULL) {
			q.push(node->left);
			++lvl2;						//��� ���������� ���� ���������� ������
		}
		if (node->right != NULL) {
			q.push(node->right);
			++lvl2;
		}

		++cur_lvl;
		if (cur_lvl == lvl1)			//����������� ���� ����
		{
			if (lvl1 > max_width)
				max_width = lvl1;

			lvl1 = lvl2;
			lvl2 = 0;
			cur_lvl = 0;
		}
	}

	return max_width;
}

/*	End CTree	*/

/*	CTreap	*/

struct CTreapNode
{
	int key;
	int priority;
	CTreapNode* left;
	CTreapNode* right;

	CTreapNode(const int& data, const int& prior) : key(data), priority(prior), left(NULL), right(NULL) {};
};

class CTreap
{
public:
	CTreap() : root(NULL){};
	~CTreap();
	void Insert(const int& key, const int& priority);
	int Width();

private:
	CTreapNode* root;

	void Split(CTreapNode* node, const int& key, CTreapNode*& left, CTreapNode*& right);
	void Delete_subtreap(CTreapNode*& node);
	void Insert(CTreapNode*& node, const int& key, const int& priority);
};

CTreap::~CTreap()
{
	Delete_subtreap(root);
}

void CTreap::Delete_subtreap(CTreapNode*& node)
{
	if (node)
	{
		Delete_subtreap(node->left);
		Delete_subtreap(node->right);
		delete node;
	}
}

void CTreap::Insert(const int& key, const int& priority)
{
	Insert(root, key, priority);
}

void CTreap::Insert(CTreapNode*& node, const int& key, const int& priority)
{
	if (node == NULL) {
		node = new CTreapNode(key, priority);
		return;
	}

	if (node->priority < priority) {
		CTreapNode* temp = new CTreapNode(key, priority);
		Split(node, key, temp->left, temp->right);
		node = temp;
		return;
	}

	if (node->key <= key) {
		Insert(node->right, key, priority);
	}
	else {
		Insert(node->left, key, priority);
	}
}

void CTreap::Split(CTreapNode* node, const int& key, CTreapNode*& left, CTreapNode*& right)
{
	if (node == NULL) {
		left = NULL;
		right = NULL;
	}
	else if (node->key <= key) {
		Split(node->right, key, node->right, right);
		left = node;
	}
	else{
		Split(node->left, key, left, node->left);
		right = node;
	}
}

int CTreap::Width()
{
	if (root == NULL)
		return 0;

	int max_width = 1;
	int lvl1 = 1;
	int lvl2 = 0;
	int cur_lvl = 0;

	queue<CTreapNode*> q;
	q.push(root);

	while (!q.empty()) {
		CTreapNode* node = q.front();
		q.pop();
		
		if (node->left != NULL) {
			q.push(node->left);
			++lvl2;
		}
		if (node->right != NULL) {
			q.push(node->right);
			++lvl2;
		}

		++cur_lvl;
		if (cur_lvl == lvl1)
		{
			if (lvl1 > max_width)
				max_width = lvl1;

			lvl1 = lvl2;
			lvl2 = 0;
			cur_lvl = 0;
		}
	}

	return max_width;
}

/*	End CTreap	*/

int main()
{
	CTree tree;
	CTreap treap;

	int n = 0;
	cin >> n;

	for (int i = 0; i < n; i++)
	{
		int key = 0;
		int priority = 0;
		cin >> key >> priority;

		tree.Insert(key);
		treap.Insert(key, priority);
	}

	cout << treap.Width() - tree.Width() << '\n';

	return 0;
}