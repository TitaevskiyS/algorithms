/*2.1 Zero of factorials*/
#include <iostream>

int FactorialsZero(int N){
	int count = 0;
	int temp = N;
	while (temp /= 5)		//evry 5 - 1 zero, 5*5 - 2 zero, etc.
		count += temp;
	return count;
}

int main(){
	int N = 0;
	std::cin >> N;
	std::cout << FactorialsZero(N) << '\n';
	//system("pause");
	return 0;
}