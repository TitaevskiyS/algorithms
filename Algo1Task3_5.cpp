/*3.5 */
#include <iostream>

int FindLost(const int* const arr, const int N)
{
	int sum = N * (N + 1) / 2;
	for (int i = 0; i < N; ++i){
		sum -= arr[i];
	}
	return sum;
}

int main()
{
	int N = 0;
	std::cin >> N;

	int* arr = new int[N];

	for (int i = 0; i < N; ++i){
		std::cin >> arr[i];
	}

	std::cout << FindLost(arr, N);

	delete[] arr;
	//system("pause");
	return 0;
}