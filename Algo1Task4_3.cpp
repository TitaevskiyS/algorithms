/*4.3 ����� ����������� �������� �� m*log(n)*/

#include <iostream>
int* Intersect(int* big_A, int* small_B, int big_n, int less_m, int& out_size)
{
	int* out = new int[less_m];		//������ ����������� <= ������� �������� ���������
	out_size = 0;

	for (int i = 0; i < less_m; i++){	//���������� �������� ����� �� big_A �� ���� ��������� small_B (m ���)
		int first = 0;
		int last = big_n;
		while (first < last){
			int mid = (first + last) / 2;
			if (small_B[i] <= big_A[mid]){
				last = mid;
			}
			else {
				first = mid + 1;
			}
		}
		if (first != big_n && big_A[first] == small_B[i]){
			out[out_size] = small_B[i];
			++out_size;
		}
	}

	return out;
}

int main()
{
	int n = 0;
	int m = 0;
	std::cin >> n >> m;
	if (n < m || n == 0 || m == 0){
		return 0;
	}

	int* A = new int[n];
	int* B = new int[m];

	for (int i = 0; i < n; i++){
		std::cin >> A[i];
	}
	for (int i = 0; i < m; i++){
		std::cin >> B[i];
	}

	int size_C = 0;
	int* out_C = Intersect(A, B, n, m, size_C);

	for (int i = 0; i < size_C; i++){
		std::cout << out_C[i] << ' ';
	}

	delete[] out_C;
	delete[] A;
	delete[] B;
	//system("pause");
	return 0;
}