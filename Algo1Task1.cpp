/*1.1 Lucky tickets*/
#include <iostream>
#include <algorithm>


int CountOfLucky1(void)
{
	int count = 0;
	int CT = 0;

	for (int T = 0; T <= 13; ++T)
	{
		CT = 0;
		for (int i = std::max(0, T - 18); i <= std::min(9, T); ++i){
			CT += std::min(9, T - i) - std::max(0, T - i - 9) + 1;
		}
		count += CT * CT;
	}
	count *= 2;
	return count;
}


int CountOfLucky2(void)
{
	int count = 0;
	int a6 = 0;
	for (int a1 = 0; a1 < 10; a1++)
		for (int a2 = 0; a2 < 10; a2++)
			for (int a3 = 0; a3 < 10; a3++)
				for (int a4 = 0; a4 < 10; a4++)
					for (int a5 = 0; a5 < 10; a5++) {
						a6 = (a1 + a2 + a3) - (a4 + a5);
						if (a6 >= 0 && a6 <= 9)
							++count;
					}
	return count;
}
int main()
{
	std::cout << CountOfLucky1() << '\n';
	std::cout << CountOfLucky2() << '\n';

	system("pause");
	return 0;
}