/*4 ����� k-�� ���������� ����������*/

#include <iostream>

int Partition(int* a, int left, int right)
{
	if (a[(right + left) / 2] > a[left] && a[(right + left) / 2] > a[right]){
		if (a[right] > a[left]){
			std::swap(a[left], a[right]);
		}
	}
	else if (a[(right + left) / 2] < a[left] && a[(right + left) / 2] < a[right])
	{
		if (a[right] < a[left]){
			std::swap(a[left], a[right]);
		}
	}

	int o = a[left];
	int i = right;
	int j = right;

	while (j > left){
		while (a[j] <= o && j > left)
			--j;

		if (j > left){
			std::swap(a[i], a[j]);
			--i;
			--j;
		}
	}
	std::swap(a[left], a[i]);
	return i;
}

int Find_order_statistic(int* a, int n, int k)
{
	int left = 0;
	int right = n - 1;

	while (true) {
		int mid = Partition(a, left, right);

		if (mid < k)
			left = mid + 1;
		else if (mid > k)
			right = mid - 1;
		else return a[k];
	}
}

int main()
{
	int n = 0;
	int k = 0;

	std::cin >> n >> k;

	int* arr = new int[n];

	for (int i = 0; i < n; ++i)
		std::cin >> arr[i];

	std::cout << Find_order_statistic(arr, n, k);

	delete[] arr;

	//std::cout << '\n';
	//system("Pause");
	return 0;
}