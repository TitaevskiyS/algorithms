/*3.1 Max sum. Output indexes of 2 array A, B - (i0 , j0) A[i0] + B[j0] = max{A[i] + B[j]} i <= j*/
#include <iostream>

void IndexMax(const int* const A, const int* const B, const int maxN, int& i_0, int& j_0){
	i_0 = 0;
	j_0 = 0;
	int	i_max = i_0;

	int max_sum = A[i_0] + B[j_0];

	for (int j = 1; j < maxN; ++j){
		if (A[j] > A[i_max]){
			i_max = j;						//remember pos current max elem in array A (it will be i_0 or i_max)
		}

		if (A[i_max] + B[j] > max_sum){		//if true - update max sum and current pos
			i_0 = i_max;
			j_0 = j;
			max_sum = A[i_0] + B[j_0];
		}
	}
}

void ReadArray(int* const array, int maxN){
	for (int i = 0; i < maxN; ++i){
		std::cin >> array[i];
	}
}

int main(){
	int i_0 = 0,
		j_0 = 0;
	int maxN = 0;
	std::cin >> maxN;

	if (maxN < 1)
		return 1;

	int* A = new int[maxN];
	int* B = new int[maxN];
	
	ReadArray(A, maxN);
	ReadArray(B, maxN);

	IndexMax(A, B, maxN, i_0, j_0);

	std::cout << i_0 << ' ' << j_0;
	//std::cout << '\n';

	delete[] A;
	delete[] B;
	//system("pause");
	return 0;
}