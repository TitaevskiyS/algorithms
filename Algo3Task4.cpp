#include <iostream>
#include <algorithm>

using namespace std;

struct CAVLNode
{
	int key;
	int height;
	CAVLNode* left;
	CAVLNode* right;

	CAVLNode(int data) : key(data), height(1), left(NULL), right(NULL) {};
};

class CAVLTree
{
public:
	CAVLTree() : root(NULL) {};
	~CAVLTree() { Delete_subtree(root); };
	void Add(int key);
	void Remove(int key);
	int Height() const { return Height(root); };

private:
	CAVLNode* root;

	void Add(CAVLNode*& node, int key);
	void Balance(CAVLNode*& node);
	int Height(const CAVLNode* node) const { return node == NULL ? 0 : node->height; };
	int Delta(const CAVLNode* node) const { return Height(node->right) - Height(node->left); };
	void Rotate_left(CAVLNode*& node);
	void Rotate_right(CAVLNode*& node);
	void Fix_height(CAVLNode*& node);
	CAVLNode* Remove_min(CAVLNode*& node);
	void Remove(CAVLNode*& node, int key);
	void Delete_subtree(CAVLNode*& node);
};

void CAVLTree::Delete_subtree(CAVLNode*& node)
{
	if (node)
	{
		Delete_subtree(node->left);
		Delete_subtree(node->right);
		delete node;
	}
}

void CAVLTree::Add(int key)
{
	Add(root, key);
}

void CAVLTree::Add(CAVLNode*& node, int key)
{
	if (node == NULL) {
		node = new CAVLNode(key);
		return;
	}

	if (node->key <= key)
		Add(node->right, key);
	else
		Add(node->left, key);

	Balance(node);
}

void CAVLTree::Balance(CAVLNode*& node)
{
	Fix_height(node);

	const int d = Delta(node);

	if (d == 2) {
		if (Delta(node->right) == -1)
			Rotate_right(node->right);
		Rotate_left(node);
	}
	else if (d == -2) {
		if (Delta(node->left) == 1)
			Rotate_left(node->left);
		Rotate_right(node);
	}
}

void CAVLTree::Fix_height(CAVLNode*& node)
{
	node->height = max(Height(node->left), Height(node->right)) + 1;
}

void CAVLTree::Rotate_left(CAVLNode*& node)
{
	CAVLNode* temp = node->right;

	node->right = temp->left;
	temp->left = node;

	Fix_height(node);
	Fix_height(temp);
	node = temp;
}

void CAVLTree::Rotate_right(CAVLNode*& node)
{
	CAVLNode* temp = node->left;

	node->left = temp->right;
	temp->right = node;

	Fix_height(node);
	Fix_height(temp);
	node = temp;
}

void CAVLTree::Remove(int key)
{
	Remove(root, key);
}

void CAVLTree::Remove(CAVLNode*& node, int key)
{
	if (node == NULL)
		return;

	if (node->key < key) {
		Remove(node->right, key);
	}
	else if (node->key > key) {
		Remove(node->left, key);
	}
	else {
		CAVLNode* l = node->left;
		CAVLNode* r = node->right;

		if (r && l) {
			CAVLNode* temp = Remove_min(node->right);
			node->key = temp->key;
			delete temp;
			return;
		}

		delete node;

		if (r == NULL) {
			node = l;
			return;
		}
		else if (l == NULL) {
			node = r;
			return;
		}
	}

	Balance(node);
}

CAVLNode* CAVLTree::Remove_min(CAVLNode*& node)
{
	if (node->left == NULL) {
		CAVLNode* temp = node;
		node = node->right;
		return temp;
	}

	CAVLNode* res = Remove_min(node->left);
	Balance(node);

	return res;
}

/*		CAVLTree	*/

int main()
{
	CAVLTree AVLTree;

	int temp = 0;
	cin >> temp;

	while (!cin.eof()) {
		if (temp > 0) {
			AVLTree.Add(temp);
		}
		else if (temp < 0) {
			AVLTree.Remove(abs(temp));
		}

		cin >> temp;
	}

	cout << AVLTree.Height() << '\n';

	return 0;
}