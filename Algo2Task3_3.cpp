#include <iostream>

struct CPoint
{
	int x;		//����������
	int flag;	//1 - ������, -1 - ����� �������
};

bool operator < (const CPoint& a, const CPoint& b)
{
	return a.x < b.x;
}

template<typename T>
void merge(T* a, int left, int split, int right)
{
	int i = left;			//������ 1 �������
	int j = split + 1;		//������ 2 �������
	int pos = 0;

	T* temp = new T[right - left + 1];

	for (; i <= split && j <= right;)
		if (a[i] < a[j])
			temp[pos++] = a[i++];
		else
			temp[pos++] = a[j++];
	
	if (i > split)					//���������� �������
		for (; j <= right;)
			temp[pos++] = a[j++];
	else
		for (; i <= split;)
			temp[pos++] = a[i++];

	for (pos = 0; pos < right - left + 1; ++pos)	//����������� � �������� ������
		a[left + pos] = temp[pos];

	delete[] temp;
}

template<typename T>
void merge_sort(T* arr, int left, int right)
{
	if (left < right)
	{
		int split = (left + right) / 2;
		merge_sort<T>(arr, left, split);
		merge_sort<T>(arr, split + 1, right);
		merge<T>(arr, left, split, right);
	}
}

int Calc_color_lenth(CPoint* a, int n)
{
	int flag = 0;
	int len = 0;

	for (int i = 0; i < n - 1; i++){
		flag += a[i].flag;
		if (flag > 0)
			len += a[i + 1].x - a[i].x;
	}

	return len;
}

int main()
{
	int n = 0;
	std::cin >> n;

	CPoint* unsorted = new CPoint[2 * n];

	for (int i = 0; i < n; i++)
	{
		std::cin >> unsorted[i * 2].x >> unsorted[i * 2 + 1].x;

		unsorted[i * 2].flag = 1;			//������
		unsorted[i * 2 + 1].flag = -1;			//�����
	}

	merge_sort<CPoint>(unsorted, 0, 2 * n - 1);

	std::cout << Calc_color_lenth(unsorted, 2 * n);

	delete[] unsorted;

	//system("Pause");
	return 0;
}