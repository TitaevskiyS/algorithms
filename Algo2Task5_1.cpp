/*5.1 ������ k ��������� ������� ������������������*/

#include <iostream>

using namespace std;

template<typename T>
void merge(T* a, int left, int split, int right)
{
	int i = left;			//������ 1 �������
	int j = split + 1;		//������ 2 �������
	int pos = 0;

	T* temp = new T[right - left + 1];

	for (; i <= split && j <= right;)
		if (a[i] < a[j])
			temp[pos++] = a[i++];
		else
			temp[pos++] = a[j++];

	if (i > split)					//���������� �������
		for (; j <= right;)
			temp[pos++] = a[j++];
	else
		for (; i <= split;)
			temp[pos++] = a[i++];

	for (pos = 0; pos < right - left + 1; ++pos)	//����������� � �������� ������
		a[left + pos] = temp[pos];

	delete[] temp;
}

template<typename T>
/*
k - ���������� ��������� � �����
res_size - ���������� ������ ����������� ���������
*/
void merge(T*a, int left, int k, T* res, int res_size)
{
	int i = left;			//������ 1 �������
	int j = 0;				//������ 2 ������� - ����� res
	int pos = 0;

	T* temp = new T[res_size];

	for (; i < left + k && j < res_size && pos < res_size;)
		if (a[i] < res[j])
			temp[pos++] = a[i++];
		else
			temp[pos++] = res[j++];

	for (; j < res_size && pos < res_size;)			//���������� �������
		temp[pos++] = res[j++];

	for (pos = 0; pos < res_size; ++pos)	//����������� � ����� res
		res[pos] = temp[pos];

	delete[] temp;

}

template<typename T>
void merge_sort(T* arr, int left, int right)
{
	if (left < right)
	{
		int split = (left + right) / 2;
		merge_sort<T>(arr, left, split);
		merge_sort<T>(arr, split + 1, right);
		merge<T>(arr, left, split, right);
	}
}

void First_k_elem(int* a, int n, int k, int* res)
{
	int i = 0;
	for (; i < n / k; i++)
		merge_sort<int>(a, i * k, (i + 1) * k - 1);		//��������� �������� n/k ������ �� k ���������

	merge_sort<int>(a, i * k, n - 1);					//��������� �������� �������

	for (int j = 0; j < k; j++)							//��������� ������ ���� � �����
		res[j] = a[j];

	int j = 1;
	for (; j < n / k; j++)							//���������������� ������� ������ � ����� ������� k
		merge<int>(a, j * k, k, res, k);

	if (n - (n / k) * k)
		merge<int>(a, j * k, n - (n / k) * k, res, k);		//������� ������ � �������
}

int main()
{
	int n = 0;
	int k = 0;

	cin >> n >> k;

	int* arr = new int[n];

	for (int i = 0; i < n; i++)
		cin >> arr[i];
	
	int* res = new int[k];

	First_k_elem(arr, n, k, res);

	for (int i = 0; i < k; i++)
		cout << res[i] << ' ';

	delete[] res;
	delete[] arr;

	//system("Pause");
	return 0;
}