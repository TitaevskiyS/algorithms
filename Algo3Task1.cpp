#include <iostream>
#include <string>
#include <vector>

#define INIT_SIZE	8
#define DELETE_NODE	"-1"
#define HASH_A1		29
#define HASH_A2		13

using namespace std;

int Hash(const string& s, int M, int a)
{
	int res = 0;

	for (size_t i = 0; i < s.length(); ++i)
		res = (res * a + s[i]) % M;

	return res;
}

class CHashTable
{
public:
	CHashTable(size_t size) : table(size, "\0"), cur_size(0) {};
	bool Add(const string& key);
	bool Delete(const string& key);
	bool Find(const string& key) const;

private:
	vector<string> table;
	int cur_size;

	void Resize();
};

bool CHashTable::Add(const string& key)
{
	if ((float)cur_size / table.size() > 2./3)
		Resize();
	
	int hash = Hash(key, table.size(), HASH_A1);
	int hash2 = 2 * Hash(key, table.size(), HASH_A2) + 1;
	int pos = -1;

	for (int i = 0; i < table.size(); ++i)
	{
		int j = (hash + i * hash2) % table.size();
		
		if (table[j] == key) {
			return false;
		}
		else if (pos == -1 && (table[j] == DELETE_NODE || table[j] == "\0")) {
			pos = j;
		}

		if (table[j] == "\0")
			break;
	}

	table[pos] = key;
	++cur_size;

	return true;
}

bool CHashTable::Delete(const string& key)
{
	int hash = Hash(key, table.size(), HASH_A1);
	int hash2 = 2 * Hash(key, table.size(), HASH_A2) + 1;

	for (int i = 0; i < table.size(); ++i)
	{
		int j = (hash + i * hash2) % table.size();

		if (table[j] == key) {
			table[j] = DELETE_NODE;
			--cur_size;
			return true;
		}
		else if (table[j] == "\0") {
			return false;
		}
	}
}

bool CHashTable::Find(const string& key) const
{
	int hash = Hash(key, table.size(), HASH_A1);
	int hash2 = 2 * Hash(key, table.size(), HASH_A2) + 1;

	for (int i = 0; i < table.size(); ++i)
	{
		int j = (hash + i * hash2) % table.size();

		if (table[j] == key) {
			return true;
		}
		else if (table[j] == "\0")
			return false;
	}
	return false;
}

void CHashTable::Resize()
{
	cur_size = 0;
	vector<string> new_table(table.size() * 2, "\0");

	swap(table, new_table);

	for (size_t i = 0; i < new_table.size(); ++i)
	{
		if (new_table[i] != DELETE_NODE && new_table[i] != "\0")
		{
			int hash = Hash(new_table[i], table.size(), HASH_A1);
			int hash2 = 2 * Hash(new_table[i], table.size(), HASH_A2) + 1;
			int pos = -1;

			for (int j = 0; j < table.size(); ++j)
			{
				int k = (hash + j * hash2) % table.size();

				if (table[k] == "\0") {
					table[k] = new_table[i];
					++cur_size;
					break;
				}
			}
		}
	}
}

int main()
{
	CHashTable string_table(INIT_SIZE);

	while (true)
	{
		string s;
		cin >> s;

		if (cin.eof())
			break;

		if (s == "+"){
			cin >> s;
			cout << (string_table.Add(s) ? "OK" : "FAIL");
		}
		else if (s == "-"){
			cin >> s;
			cout << (string_table.Delete(s) ? "OK" : "FAIL");
		}
		else if (s == "?"){
			cin >> s;
			cout << (string_table.Find(s) ? "OK" : "FAIL");
		}

		cout << '\n';
	}
	return 0;
}