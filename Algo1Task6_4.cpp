#include <iostream>
#include <stdlib.h>

#define MAX_LENGTH	100

template <typename T>
struct CNode
{
	CNode(T _data = 0, CNode* _prev = NULL) : data(_data), prev(_prev)	{};

	T data;
	CNode* prev;
};

template <typename T>
class CStack{
private:
	CNode<T>* top;
public:
	CStack() : top(NULL) {};
	CStack(const CStack& data)
	{
		if (data.top){
			CNode<T>* temp = data.top;				//�� ����������� �����
			top = new CNode<T>(temp->data, NULL);
			temp = temp->prev;
			CNode<T>* prev = top;					//�� ������ �����

			while (temp){
				prev->prev = new CNode<T>(temp->data, NULL);
				prev = prev->prev;
				temp = temp->prev;
			}
		}
	}
	~CStack()
	{
		while (top != NULL){
			CNode<T>* temp = top->prev;
			delete top;
			top = temp;
		}
	}
	bool Is_empty() const
	{
		return top == NULL;
	};
	void Push(T data)
	{
		top = new CNode<T>(data, top);
	};
	T Pop()
	{
		T res = top->data;
		CNode<T>* temp = top;
		top = top->prev;
		delete temp;
		return res;
	};
	T Look_top() const
	{
		return top->data;
	};
};

struct CData
{
	bool is_symbol;
	union
	{
		int number;
		char symbol;
	};

	CData(int data) : is_symbol(false), number(data) {};
	CData(char data) : is_symbol(true), symbol(data) {};
	void Put(int data)
	{
		is_symbol = false;
		number = data;
	}
	void Put(char data)
	{
		is_symbol = true;
		symbol = data;
	}
};

int Priority(char data)
{
	if (data == '(')
		return 1;
	else if (data == '+' || data == '-')
		return 2;
	else								//if (data == '*' || data == '/')
		return 3;
}

CStack<CData> InfToPostf(char* buffer)
{
	CStack<CData> temp;
	CStack<char> symbol;

	int cur_pos = 0;		//in buffer

	while (buffer[cur_pos]){
		if (isdigit(buffer[cur_pos])){
			int res = atoi(buffer + cur_pos);
			temp.Push(res);

			while (isdigit(buffer[cur_pos]))
				++cur_pos;
		}
		else if (buffer[cur_pos] == '('){
			symbol.Push(buffer[cur_pos]);
			++cur_pos;
		}
		else if (buffer[cur_pos] == ')'){
			while (symbol.Look_top() != '(')
				temp.Push(symbol.Pop());

			symbol.Pop();						//delete '('
			++cur_pos;
		}
		else if (symbol.Is_empty() || Priority(buffer[cur_pos]) > Priority(symbol.Look_top())){
			symbol.Push(buffer[cur_pos]);
			++cur_pos;
		}
		else{
			while (Priority(buffer[cur_pos]) <= Priority(symbol.Look_top())){
				temp.Push(symbol.Pop());
				if (symbol.Is_empty())
					break;
			}

			symbol.Push(buffer[cur_pos]);
			++cur_pos;
		}
	}

	while (!symbol.Is_empty())
		temp.Push(symbol.Pop());

	CStack<CData> returned_stack;
	while (!temp.Is_empty())
		returned_stack.Push(temp.Pop());

	return returned_stack;
}

int CalcPostfix(CStack<CData> input)
{
	CStack<int> temp;

	while (!input.Is_empty()){
		if (!input.Look_top().is_symbol)
			temp.Push(input.Pop().number);
		else{
			int res = 0;
			int right = temp.Pop();
			int left = temp.Pop();
			char symbol = input.Pop().symbol;

			switch (symbol)
			{
			case '+':
				res = left + right;
				break;
			case '-':
				res = left - right;
				break;
			case '*':
				res = left * right;
				break;
			case '/':
				res = left / right;
				break;
			}

			temp.Push(res);
		}
	}

	return temp.Pop();
}

int main()
{
	char buffer[MAX_LENGTH];

	std::cin >> buffer;
	CStack<CData> postfix = InfToPostf(buffer);

	std::cout << CalcPostfix(postfix);

	//std::cout << '\n';
	//system("pause");
	return 0;
}